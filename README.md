# Aurity coding challenge
React-Native App for searching repos and its contributors in github

### Tested in
- iPhone 8 - iOS 11
- iPhone 8 plus - iOS 11
- iPhone 5s - iOS 10
- One Plus 3T - Android 7.1.1

 ### Test steps

1 - Install dependencies by typing `yarn install`

2 - Install and launch the app by typing `react-native run-ios`or `react-native run-android`

3 - If you want to run tests just type `yarn test`

(*) You have to install previously `yarn` and `react-native` commands

### Comments on some features

- In `UserInfoScreen.js` file I've used an object called `keyToText`. I used i18n to support multilanguage in every app I use so text injection in the code is done 
using a i18n object that returns its porper string given a key. Here it was also more clean and easy to use an object to render those keys text. 

### Testing and debugging process

It's been configured [React Native Developer Tools](https://github.com/jhen0409/react-native-debugger)
so you are able to log redux state better. 
