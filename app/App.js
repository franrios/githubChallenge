import { StackNavigator } from 'react-navigation'

import * as Containers from '%containers'
import { GREY_LIGHT } from '%appStyles'

export const App = StackNavigator({
  Search: { screen: Containers.SearchContainer },
  UserInfo: { screen: Containers.UserInfoContainer },
}, {
  cardStyle: {
    backgroundColor: GREY_LIGHT,
  },
})
