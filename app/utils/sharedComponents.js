/**
 * @providesModule %sharedComponents
 *
 *
 *  sharedComponents contains useful components for the rest of the app
 *
 *  -> Hr: Is a line divisor
 *  -> SelectionButton: Component used inside events boxes as buttons for selections
 *  -> Button: Simple button only containing text
 */

 import React from 'react'
 import { View, Text } from 'react-native'
 import PropTypes from 'prop-types'

 import * as appStyles from '%appStyles'

 export const Hr = (props) => (
   <View
    style={[
      {
        height: 1,
        alignSelf: 'stretch',
        width: '100%',
        backgroundColor: props.color,
      },
      props.style,
    ]} />
 )
 Hr.propTypes = {
   color: PropTypes.string,
   style: View.propTypes.style,
 }
 Hr.defaultProps = {
   color: appStyles.GREY_DARK,
   style: null,
 }


 export const Bold = (props) => (
   <Text
    style={[{
      color: props.color || appStyles.GREY,
      fontWeight: 'bold',
      fontSize: 15,
    },
      props.style]}>
      {props.children}
    </Text>
 )
