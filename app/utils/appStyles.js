/*
* @providesModule %appStyles
*/

export const GREY = '#86939E'
export const GREY_LIGHT = '#E1E8EE'
export const GREY_DARK = '#37404B'
export const GREY_SEARCH_BAR = '#BDC6CE'
export const BLUE_GREY = '#ABBDCE'
export const WHITE_CUSTOM = '#F5F5F5'
