/**
 * @providesModule %githubApi
 */

 export const search = (query) => (
   fetch(
     `https://api.github.com/search/repositories?q=${query}`,
     {
       method: 'GET',
     }
   ).then(response => response.json())
 )

 export const getContributors = (url) => (
   fetch(url, { method: 'GET'})
    .then(response => response.json())
 )

 export const getUserInfo = (url) => (
   fetch(url, { method: 'GET'})
    .then(response => response.json())
 )
