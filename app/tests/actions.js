import should from 'should' // eslint-disable-lines

import * as actions from '../store/actions'
import types from '../store/actionTypes'

const searchResponse = require('./searchResponse.json')
const userInfoResponse = require('./userInfoResponse.json')

describe('Actions test', () => {
  describe('Search actions: saveSearch', () => {
    const result = actions.saveSearch(searchResponse)
    it('should have its proper type', () => {
      result.should.have.property('type', types.SAVE_SEARCH)
    })

    it('should have a items property', () => {
      result.should.have.property('searchResult', searchResponse)
    })
  })

  describe('User Info actions: getUserInfo', () => {
    const result = actions.getUserInfo(userInfoResponse)
    it('should have its proper type', () => {
      result.should.have.property('type', types.GET_USER_INFO)
    })

    it('should have a userInfo property', () => {
      result.should.have.property('userInfo', userInfoResponse)
    })
  })
})
