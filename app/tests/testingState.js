export default {
  search: {
    items: [
      {
        id: 54597585,
        name: 'franrios.github.io',
        full_name: 'franrios/franrios.github.io',
        contributors_url:
          'https://api.github.com/repos/franrios/franrios.github.io/contributors',
      },
    ],
    total_count: 1,
    incomplete_results: false,
  },
  userInfo: {
    login: 'franrios',
    avatar_url: 'https://avatars0.githubusercontent.com/u/7302131?v=4',
    name: 'Fran',
    bio: 'Enabling tech stuff 🕹',
    public_repos: 48,
    followers: 26,
    following: 48,
    location: 'Seville',
  },
}
