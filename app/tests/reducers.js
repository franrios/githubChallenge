import should from 'should' // eslint-disable-line

import { rootReducer } from '../store/reducers'
import types from '../store/actionTypes'
import testingState from './testingState'

const searchResponse = require('./searchResponse.json')
const userInfoResponse = require('./userInfoResponse.json')

describe('Reducers test', () => {
  describe('Search reducer', () => {
    const state = rootReducer(undefined, {
      type: types.SAVE_SEARCH,
      searchResult: searchResponse,
    })

    it('should have items property', () => {
      state.search.should.have.property('items')
    })

    const firstItem = state.search.items.shift()
    const apiFirstItem = searchResponse.items.shift()

    it('items should contain id', () => {
      firstItem.should.have.property('id', apiFirstItem.id)
    })

    it('items should contain name', () => {
      firstItem.should.have.property('name', apiFirstItem.name)
    })

    it('items should contain full_name', () => {
      firstItem.should.have.property('full_name', apiFirstItem.full_name)
    })

    it('items should contain contributors_url', () => {
      firstItem.should.have.property('contributors_url', apiFirstItem.contributors_url)
    })

    it('items should not have other properties', () => {
      firstItem.should.not.have.property('description')
    })
  })

  describe('User Info Reducer', () => {
    const state = rootReducer(undefined, {
      type: types.GET_USER_INFO,
      userInfo: userInfoResponse,
    })

    it('should have login', () => {
      state.userInfo.should.have.property('login', testingState.userInfo.login)
    })

    it('should have avatar_url', () => {
      state.userInfo.should.have.property('avatar_url', testingState.userInfo.avatar_url)
    })

    it('should have followers', () => {
      state.userInfo.should.have.property('followers', testingState.userInfo.followers)
    })

    it('should have following', () => {
      state.userInfo.should.have.property('following', testingState.userInfo.following)
    })

    it('should not have other properties', () => {
      state.userInfo.should.not.have.property('email')
    })
  })
})
