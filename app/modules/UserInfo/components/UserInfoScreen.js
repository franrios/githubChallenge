import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, ActivityIndicator } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/Ionicons'

import idx from 'idx'

import { Bold, Hr } from '%sharedComponents'
import * as appStyles from '%appStyles'

const keyToText = {
  bio: 'Bio',
  public_repos: 'Public Repos',
  followers: 'Followers',
  following: 'Following',
  location: 'Location',
}

export class UserInfoScreen extends Component {
  static navigationOptions = (props) => ({
    headerStyle: { backgroundColor: appStyles.BLUE_GREY },
    headerTitle: <Bold style={{alignSelf: 'center'}}>
                  {idx(props, _ => _.navigation.state.params.user.login) || 'UserInfo'}
               </Bold>,
    headerRight: <View />,
  })

  static propTypes = {
    getUserInfo: PropTypes.func.isRequired,
  }

  componentDidMount () {
    const { getUserInfo, navigation } = this.props
    const { url } = idx(navigation, _ => _.state.params.user)
    getUserInfo(url)
  }

  render () {
    const { userInfo, navigation } = this.props
    const {
      login,
      avatar_url,
      name,
      ...otherInfo
    } = userInfo

    if (login !== idx(navigation, _ => _.state.params.user.login)) {
      // render and activity indicator while loading
      return (
        <View style={{justifyContent: 'center', paddingTop: 30}}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
        <View style={styles.main}>
          <View style={styles.userHeader}>
            <Image style={styles.profilePic} source={{uri: avatar_url}} />
            <View style={{padding: 20}}>
              <Bold>{login}</Bold>
              <Text>{name}</Text>
            </View>
          </View>
          <View style={styles.infoHeader}>
            <Icon name="ios-information-circle-outline" size={20} color={appStyles.GREY_DARK} />
            <Bold style={{ padding: 5}} color={appStyles.GREY_DARK}>Info</Bold>
          </View>
          <Hr style={{width: '80%', alignSelf: 'center'}} />
          <View style={{padding: 10, width: '80%'}}>
          {
            Object.keys(otherInfo)
            .filter(key => otherInfo[key]) // If there's no value it's not rendered
            .map(key => (
              <Text key={key} style={{paddingVertical: 2}}>
                <Bold color={appStyles.GREY_DARK}>{keyToText[key]}</Bold>
                : {otherInfo[key]}
              </Text>
            ))
          }
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    borderRadius: 5,
    backgroundColor: appStyles.WHITE_CUSTOM,
    marginTop: 6,
    alignItems: 'center',
    padding: 10,
    margin: 20,
  },
  userHeader: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 5,
  },
  profilePic: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  infoHeader: {
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginLeft: 15,
  },
})
