import { connect } from 'react-redux'

import { getUserInfo } from '%thunks'

import { UserInfoScreen } from '../components'

const mapDispatchToProps = {
  getUserInfo,
}

const mapStateToProps = state => ({
  userInfo: state.userInfo,
})

export const UserInfoContainer = connect(mapStateToProps, mapDispatchToProps)(UserInfoScreen)
