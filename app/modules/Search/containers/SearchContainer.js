import { connect } from 'react-redux'
import idx from 'idx'

import { search } from '%thunks'

import { SearchScreen } from '../components'

// Using this way mapDispatchToProps, action is dispatched
const mapDispatchToProps = {
  search,
}

const mapStateToProps = state => ({
  searchResult: idx(state, _ => _.search.items),
})

export const SearchContainer = connect(mapStateToProps, mapDispatchToProps)(SearchScreen)
