import React, { Component } from 'react'
import { FlatList, View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import { Bold } from '%sharedComponents'
import * as appStyles from '%appStyles'

import { RepoBox } from './RepoBox'
import { SearchBar } from './SearchBar'

export class SearchScreen extends Component {
  static propTypes = {
    search: PropTypes.func.isRequired,
    searchResult: PropTypes.array.isRequired, // eslint-disable-line
  }

  static navigationOptions = () => ({
    headerStyle: { backgroundColor: appStyles.BLUE_GREY },
    headerTitle: <Bold style={{alignSelf: 'center'}}>Search for a repo</Bold>,
  })

  render () {
    const { searchResult, search, navigation } = this.props

    return (
      <View style={styles.main}>
        <SearchBar onSubmit={text => search(text)} />
        <FlatList
          style={styles.list}
          data={searchResult}
          renderItem={({ item }) => (
            <RepoBox
              {...item}
              onPressUser={(user) => navigation.navigate('UserInfo', { user })} />
          )}
          keyExtractor={(item, index) => index} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    marginTop: 5,
  },
  list: {
    flex: 1,
    width: '95%',
  },
})
