import React, { Component } from 'react'
import { View, TextInput, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types'

import * as appStyles from '%appStyles'

export class SearchBar extends Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this.state = {
      input: '',
    }
  }

  render () {
    const { input } = this.state
    const { onSubmit } = this.props

    return <View style={styles.main}>
        <Icon name="md-search" size={20} color={appStyles.GREY} style={{marginVertical: 5, marginHorizontal: 12}} />
        <TextInput
          style={{ color: appStyles.GREY, width: '80%', fontSize: 18}}
          value={input}
          defaultValue="name of a repo..."
          autoCorrect={false}
          clearTextOnFocus
          autoCapitalize={'none'}
          onChangeText={(value) => this.setState({input: value})}
          onSubmitEditing={() => onSubmit(input)}
        />
      </View>
  }
}

const styles = StyleSheet.create({
  main: {
    height: 40,
    width: '90%',
    backgroundColor: appStyles.GREY_SEARCH_BAR,
    borderRadius: 5,
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
})
