import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import moment from 'moment'
import Icon from 'react-native-vector-icons/Ionicons'

import { Bold, Hr } from '%sharedComponents'
import { getContributors } from '%githubApi'
import { GREY_DARK, WHITE_CUSTOM } from '%appStyles'

export class RepoBox extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    full_name: PropTypes.string.isRequired,
    onPressUser: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this.state = {
      showContributors: false,
      contributors: null,
      timestamp: null,
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.contributors_url !== this.props.contributors_url) {
      // Reset the state if it's not the same repo
      this.setState({
        showContributors: false,
        contributors: null,
        timestamp: null,
      })
    }
  }

  _getContributors () {
    const { showContributors, timestamp } = this.state
    const { contributors_url } = this.props

    if (!showContributors) {
      if (!timestamp || moment().diff(timestamp, 'seconds') > 30) { // Results are cached 30 secs
        getContributors(contributors_url)
        .then(response => this.setState({
          showContributors: true,
          contributors: response,
          timestamp: moment.now(),
        }))
      } else { // Last request was done before and it's not older than 30 secs
        this.setState({ showContributors: true })
      }
    } else {
      this.setState({ showContributors: false })
    }
  }

  render () {
    const { contributors, showContributors } = this.state
    const { name, full_name, onPressUser } = this.props
    const buttonText = `${showContributors ? 'Hide' : 'Show'} contributors`
    return <View style={styles.main}>
        <View style={styles.header}>
          <Text numberOfLines={1} style={{ color: GREY_DARK, maxWidth: '80%' }}>
            {full_name.split('/').shift()}/
            <Bold color={GREY_DARK}>{name}</Bold>
          </Text>
          <TouchableOpacity onPress={() => this._getContributors()}>
            <Text style={{ fontSize: 10 }}>{buttonText}</Text>
          </TouchableOpacity>
        </View>
        {showContributors && contributors ? <View style={{ flex: 1 }}>
            <View style={styles.contributorsHeader}>
              <Icon name="ios-people-outline" size={30} color={GREY_DARK} style={{ paddingHorizontal: 10 }} />
              <Text>Contributors</Text>
            </View>
            <Hr style={{ width: 280 }} />
            {contributors.map(({ login, url }) => (
              <TouchableOpacity
                key={login}
                style={{ padding: 3 }}
                onPress={() => onPressUser({ login, url })}>
                <Bold style={{ fontSize: 13 }} color={GREY_DARK}>
                  {login}
                </Bold>
              </TouchableOpacity>
            ))}
          </View> : <View />}
      </View>
  }
}


const styles = StyleSheet.create({
  main: {
    borderRadius: 5,
    backgroundColor: WHITE_CUSTOM,
    marginVertical: 6,
    alignItems: 'center',
  },
  header: {
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
  },
  contributorsHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
})
