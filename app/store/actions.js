/**
 * @providesModule %actions
 */

 import types from './actionTypes'

// --------- SEARCH --------- //

 export const saveSearch = (searchResult) => ({
   type: types.SAVE_SEARCH,
   searchResult,
 })


// -------- USER INFO ------- //

 export const getUserInfo = (userInfo) => ({
   type: types.GET_USER_INFO,
   userInfo,
 })

