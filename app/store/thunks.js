/*
*  @providesModule %thunks
*
*  Thunks handle async actions and dispatch after promise is resolve
*/

import * as actions from '%actions'
import * as githubApi from '%githubApi'

export function search (query) {
  return (dispatch) => {
    try {
      githubApi.search(query)
      .then(response => dispatch(actions.saveSearch(response)))
    } catch (err) {
      console.err(err) // eslint-disable-line
    }
  }
}

export function getUserInfo (url) {
  return (dispatch) => {
    try {
      githubApi.getUserInfo(url)
      .then(response => dispatch(actions.getUserInfo(response)))
    } catch (err) {
      console.err(err) // eslint-disable-line
    }
  }
}
