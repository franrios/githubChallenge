export default {
  // ------- SEARCH ------ //

  SAVE_SEARCH: 'SAVE_SEARCH',

  // ------- USER INFO ---- //

  GET_USER_INFO: 'GET_USER_INFO',
}
