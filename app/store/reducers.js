import types from './actionTypes'
import initialState from './initialState'

function searchReducer (state, action) {
  const { type, ...payload } = action

  switch (type) {
    case types.SAVE_SEARCH:
      const items = payload.searchResult.items.map(
        ({ id, name, full_name, contributors_url }) => ({ id, name, full_name, contributors_url }))
      return { ...state, ...payload.searchResult, items }
    default:
      return state
  }
}

function userInfoReducer (state, action) {
  const { type, ...payload } = action

  switch (type) {
    case types.GET_USER_INFO:
      const { // Extracts those properties needed
        login,
        avatar_url,
        name,
        bio,
        public_repos,
        followers,
        following,
        location,
      } = payload.userInfo
      return {
        ...state,
        login,
        avatar_url,
        name,
        bio,
        public_repos,
        followers,
        following,
        location,
      }
    default:
      return state
  }
}

export function rootReducer (state = initialState, action) {
  return {
    search: searchReducer(state.search, action),
    userInfo: userInfoReducer(state.userInfo, action),
  }
}
